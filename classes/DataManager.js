class DataManager {
    constructor() {
        this.data = {};
    }

    addData(data, dataId) {
        this.data[dataId] = data;
    }

    getData(dataId) {
        return this.data[dataId];
    }

        // About
//      Returns the number of records matching a specified criteria.
//      If a given value is not the same as the specified value, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
        // Parameters
//        scope:
//            key: the field name, eg. 'Distributor'
//            value: the value to scope to, eg. '20th Century Fox'
//        filter:
//            type: the type of filter to be applied. eg. 'greaterThan'
//            key: the key to search within, eg. 'Worldwide_Gross'
//            value: the value to be used with the filter type. eg. 20
//        dataId: the id of the data object to search within, eg. 'movies'
//
    // Returns
//      value: the number of records counted
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getCount(options) {
        let data = this.getData(options.dataId);
        // Initialize filter as an empty object if it doesn't exist (error prevention)
        if (options.filter === undefined) options.filter = {};
        if (options.scope === undefined) options.scope = {};

        let row = {};
        let count = 0;
        let excluded = {};

        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope === 'undefined' || row[options.scope.key] === options.scope.value) {
                // The value searched for must match the variable type of the filterValue
                let variableType = typeof(options.filter.value);
                if ( variableType &&
                     typeof(row[options.filter.key]) != variableType ) {
                    // eg. If the type is a string, 'NotAString' gets passed as the second parameter
                    this._addExcludedCount(excluded, 'NotA' + variableType.charAt(0).toUpperCase() + variableType.slice(1));

                    // Skip this row
                    continue;
                }

                // Operations
                if (options.filter.type === 'greaterThan' &&
                    row[options.filter.key] > options.filter.value) {
                    count++;
                }
                else if (options.filter.type === 'smallerThan' &&
                    row[options.filter.key] < options.filter.value) {
                    count++;
                }
                else if (options.filter.type === 'equalTo' &&
                    row[options.filter.yey] === options.filter.value) {
                    count++;
                }

                if (options.filter.key === undefined) {
                    count++;
                }
            }
        }

        return {
            value: count,
            excluded: excluded,
        }
    }

    // About
//      Returns the max value of a set of records.
//      If a given value is not a number, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
    // Parameters
//        scope:
//          key: the field name, eg. 'Distributor'
//          value: the value to scope to, eg. '20th Century Fox'
//        valueOf: the value to search the max of, eg. 'Worldwide_Gross'
//        dataId: the id of the data to search within, eg. 'movies'
//
    // Returns
//      value: the calculated max
//      info: the information of the row that the maximum was taken from
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getMax(options) {
        let data = this.getData(options.dataId);
        // Initialize scope as an empty object if it doesn't exist (error prevention)
        if (options.scope === undefined) options.scope = {};

        let row = {};
        let max = null;
        let maxInfo = null;
        let excluded = {};

        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope.key === 'undefined' || row[options.scope.key] === options.scope.value) {
                // Check if value is a number, otherwise we can't determine the max
                if ( typeof(row[options.valueOf]) != 'number' ) {
                    this._addExcludedCount(excluded, 'NotANumber');

                    // Skip this row
                    continue;
                }

                // If max is not set or if it's lower, then set it to the current value
                if (max === null || max < row[options.valueOf]) {
                    max = row[options.valueOf];
                    maxInfo = row;
                }
            }
        }

        return {
            value: max,
            info: maxInfo,
            excluded: excluded,
        }
    }

    // About
//      Returns the minimum value of a set of records.
//      If a given value is not a number, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
    // Parameters
//        scope:
//          key: the field name, eg. 'Distributor'
//          value: the value to scope to, eg. '20th Century Fox'
//        valueOf: the value to search the min of, eg. 'Worldwide_Gross'
//        dataId: the id of the data to search within, eg. 'movies'
//
    // Returns
//      value: the calculated min
//      info: the information of the row that the minimum was taken from
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getMin(options) {
        let data = this.getData(options.dataId);
        if (options.scope === undefined) options.scope = {};

        let row = {};
        let min = null;
        let minInfo = null;
        let excluded = {};
        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope.key === 'undefined' || row[options.scope.key] === options.scope.value) {
                // Check if value is a number, otherwise we can't determine the min
                if ( typeof(row[options.valueOf]) != 'number' ) {
                    this._addExcludedCount(excluded, 'NotANumber');

                    // Skip this row
                    continue;
                }

                if ( row[options.valueOf] === options.exclude ) {
                    this._addExcludedCount(excluded, 'ValueExcluded');

                    // Skip this row
                    continue;
                }

                // If min is not set or if it's lower, then set it to the current value
                if (min === null || min > row[options.valueOf]) {
                    min = row[options.valueOf];
                    minInfo = row;
                }
            }
        }

        return {
            value: min,
            info: minInfo,
            excluded: excluded,
        }
    }

    // About
//      Returns the sum of a set of records.
//      If a given value is not a number, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
    // Parameters
//        scope:
//          key: the field name, eg. 'Distributor'
//          value: the value to scope to, eg. '20th Century Fox'
//        valueOf: the value to search the sum of, eg. 'Worldwide_Gross'
//        dataId: the id of the data to search within, eg. 'movies'
//
    // Returns
//      value: the calculated sum
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getSum(options) {
        let data = this.getData(options.dataId);
        if (options.scope === undefined) options.scope = {};
        let sumValue = 0;
        let row = {};
        let excluded = {};
        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope.key === 'undefined' || row[options.scope.key] === options.scope.value) {
                // Check if value is a number, otherwise we can't determine the min
                if ( typeof(row[options.valueOf]) != 'number' ) {
                    this._addExcludedCount(excluded, 'NotANumber');

                    // Skip this row
                    continue;
                }

                if ( row[options.valueOf] === options.exclude ) {
                    this._addExcludedCount(excluded, 'ValueExcluded');

                    // Skip this row
                    continue;
                }

                sumValue = sumValue + row[options.valueOf];

            }
        }

        return {
            value: sumValue,
            excluded: excluded,
        }
    }

    // About
//      Returns the average value of a set of records.
//      If a given value is not a number, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
    // Parameters
//        scope:
//          key: the field name, eg. 'Distributor'
//          value: the value to scope to, eg. '20th Century Fox'
//        valueOf: the value to search the average of, eg. 'Worldwide_Gross'
//        dataId: the id of the data to search within, eg. 'movies'
//
    // Returns
//      value: the calculated average
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getAverage(options) {
        let data = this.getData(options.dataId);
        if (options.scope === undefined) options.scope = {};
        let count = 0;
        let sumValue = 0;
        let average = 0;
        let row = {};
        let excluded = {};
        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope.key === 'undefined' || row[options.scope.key] === options.scope.value) {
                // Check if value is a number, otherwise we can't determine the min
                if ( typeof(row[options.valueOf]) != 'number' ) {
                    this._addExcludedCount(excluded, 'NotANumber');

                    // Skip this row
                    continue;
                }

                if ( row[options.valueOf] === options.exclude ) {
                    this._addExcludedCount(excluded, 'ValueExcluded');

                    // Skip this row
                    continue;
                }

                sumValue = sumValue + row[options.valueOf];
                count++;
            }
        }
 
        average = sumValue/count; 
 
        return {
            value: average,
            excluded: excluded,
        }
    }

    // PRIVATE FUNCTIONS

    // Mainly added for the purposes of seeing how many values are excluded
    _addExcludedCount(excluded, value) {
        if (!excluded[value]) {
            excluded[value] = {
                count: 0,
            };
        }

        excluded[value].count++;
    }
}