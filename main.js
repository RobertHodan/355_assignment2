function main() {
    let dataManager = new DataManager();

    // Debugging purposes
    this.dataManager = dataManager;

    let dataUrl = "data/movies_IMDB.json";
    if (window.location.origin === "file://") {
        dataUrl = 'http://www.sfu.ca/~rhodan/iat355/movies_IMDB.json';
    }

    // movies_IMDB.json originally retrieved from the course files, via canvas
    d3.json(dataUrl, function(data) {
        dataManager.addData(data, 'movies');

        jsonLoaded(dataManager);
    });
}

// Perform tasks after the json file was loaded
function jsonLoaded(dataManager) {

// Count how many records there are
    let foxMoreThan20k = dataManager.getCount({
        scope: {
          key: 'Distributor',
          value: '20th Century Fox',
        },
        filter: {
          key: 'Worldwide_Gross',
          type: 'greaterThan',
          value: 20000,
        },
        dataId: 'movies',
    });

    console.log("Number of 20th Century Fox titles that reach a Worldwide_Gross of over 20,000 dollars: "
               + foxMoreThan20k.value)

// Count the total sum of gross profits there are
	let totalSum = dataManager.getSum({
		scope: {
			key: 'Distributor',
			value:'20th Century Fox',
		},
		valueOf: 'Worldwide_Gross',
		dataId: 'movies',
	});

	console.log("The total sum of gross profits made from movies distributed by 20th Century Fox: " + totalSum.value);

  // Find which movie from 20th Century Fox made the least gross profits
  let min = dataManager.getMin({
    scope: {
      key: 'Distributor',
      value:'20th Century Fox',
    },
    valueOf: 'Worldwide_Gross',
    dataId: 'movies',
  });

  console.log("The lowest gross profit made by a movie distributed from 20th Century Fox: " + min.value);

  // Find which movie from 20th Century Fox made the most gross profits
  let max = dataManager.getMax({
    scope: {
      key: 'Distributor',
      value:'20th Century Fox',
    },
    valueOf: 'Worldwide_Gross',
    dataId: 'movies',
  });

  console.log("The highest gross profit made by a movie distributed from 20th Century Fox: " + max.value);

   // Find the average gross profit made by movies from 20th Century Fox
  let average = dataManager.getAverage({
    scope: {
      key: 'Distributor',
      value:'20th Century Fox',
    },
    valueOf: 'Worldwide_Gross',
    dataId: 'movies',
  });

  console.log("The average gross profit made from movies distributed by 20th Century Fox: " + average.value);

    // State where data was obtained from
    console.log("\n" + "The data (movies_IMDB.json) was retrieved from the course files, via canvas")

}